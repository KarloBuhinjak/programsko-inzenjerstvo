import requests

url = "https://divanscore.p.rapidapi.com/tournaments/get-standings"

querystring = {"tournamentId":"17","seasonId":"29415"}

headers = {
	"X-RapidAPI-Key": "1b2777c8bamsh475e20078b5d90ap1027eejsn898eaee9a4d6",
	"X-RapidAPI-Host": "divanscore.p.rapidapi.com"
}

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)