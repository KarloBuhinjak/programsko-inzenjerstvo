from csv import DictReader


with open("ex2-text.csv", 'r') as f:
    dict_reader = DictReader(f)

    list_of_dict = list(dict_reader)

    print(list_of_dict)
